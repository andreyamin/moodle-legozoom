<?php

/**
 * Access Code Management Block
 *
 * @package    block_accesscode
 * @copyright  2014 edubit.com.br
 * @author     Andre Yamin <andre@edubit.com.br>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
 
require_once('../../config.php');


require_login();

global $DB, $OUTPUT, $PAGE, $USER;
 
// Check for all required variables.
$courseid = required_param('courseid', PARAM_INT);
$blockid = required_param('blockid', PARAM_INT);
$lotid = required_param('lotid', PARAM_INT);
 
 
if (!$course = $DB->get_record('course', array('id' => $courseid))) {
    print_error('invalidcourse', 'block_accesscode', $courseid);
}
 
require_login($course);
$PAGE->set_url('/blocks/accesscode/lot.php', array('id' => $courseid));
$PAGE->set_pagelayout('standard');
$PAGE->set_heading('Detalhamento do lote');

$settingsnode = $PAGE->settingsnav->add(get_string('accesscode', 'block_accesscode'));
$editurl = new moodle_url('/blocks/accesscode/lot.php', array('courseid' => $courseid, 'blockid' => $blockid, 'lotid'=> $lotid));
$editnode = $settingsnode->add(get_string('manageaccesscode', 'block_accesscode'), $editurl);
$editnode->make_active();


$site = get_site();

echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('lotcodemanagement','block_accesscode')); 
// Table
$codes = $DB->get_records('block_accesscode_codes', array('lotid'=>$lotid));
$data = array();
foreach($codes as $code) {
    $line = array();
    if ($code->userid != 0) {
        $fullname = $DB->get_field('user', 'firstname', array('id'=>$code->userid)) . ' ' . $DB->get_field('user', 'lastname', array('id'=>$code->userid));
        $line[] = html_writer::link(new moodle_url('/user/profile.php', array('id'=>$code->userid)), $fullname);
    } else {
        $line[] = 'Livre';
    }
    $line[] = $code->accesscode;

    //$buttons = array();
    //$buttons[] = html_writer::link(new moodle_url('/blocks/accesscode/edit.php', array('delete'=>'1','courseid'=>$courseid, 'blockid'=>$blockid, 'lotid'=>$lot->id)), 
    //    html_writer::empty_tag('img', array('src'=>$OUTPUT->pix_url('t/delete'), 'alt'=>get_string('delete'), 'class'=>'iconsmall')));
    //$line[] = implode(' ', $buttons);
    $data[] = $line;
}
$table = new html_table();
$table->head = array('Usuário','Código de acesso');
$table->data = $data;
echo html_writer::table($table);
$context = context_system::instance();
echo $OUTPUT->single_button(new moodle_url('/blocks/accesscode/index.php', array('blockid'=>$blockid, 'courseid'=>$courseid)), 'Voltar para a lista de lotes');
echo $OUTPUT->footer();
?>