<?php

/**
 * Configuration for the Bootstrapcustom theme
 *
 *
 * @package   Moodle Bootstrapcustom theme
 * @copyright 2012 Bas Brands. www.sonsbeekmedia.nl
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$THEME->name = 'bootstrapcustom';

$THEME->parents = array('bootstrap','base');

$THEME->sheets = array('bootstrapcustom');

$THEME->csspostprocess = 'bootstrapcustom_user_settings';

$THEME->rendererfactory = 'theme_overridden_renderer_factory';

