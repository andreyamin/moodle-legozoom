<?php

/**
 * Settings for the bootstrapcustom theme
 */

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {
    
    //This is the note box for all the settings pages
    $name = 'theme_bootstrapcustom/notes';
    $heading = get_string('notes', 'theme_bootstrapcustom');
    $information = get_string('notesdesc', 'theme_bootstrapcustom');
    $setting = new admin_setting_heading($name, $heading, $information);
    $settings->add($setting);
	
	$name = 'theme_bootstrapcustom/enablejquery';
    $title = get_string('enablejquery','theme_bootstrapcustom');
    $description = get_string('enablejquerydesc', 'theme_bootstrapcustom');
    $default = '1';
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default);
    $settings->add($setting);
    
    $name = 'theme_bootstrapcustom/enableglyphicons';
    $title = get_string('enableglyphicons','theme_bootstrapcustom');
    $description = get_string('enableglyphiconsdesc', 'theme_bootstrapcustom');
    $default = '0';
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default);
    $settings->add($setting);
    
    $name = 'theme_bootstrapcustom/shortennavbar';
    $title = get_string('shortennavbar','theme_bootstrapcustom');
    $description = get_string('shortennavbardesc', 'theme_bootstrapcustom');
    $default = '0';
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default);
    $settings->add($setting);
    
    $name = 'theme_bootstrapcustom/showpurgecaches';
    $title = get_string('showpurgecaches','theme_bootstrapcustom');
    $description = get_string('showpurgecachesdesc', 'theme_bootstrapcustom');
    $default = '0';
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default);
    $settings->add($setting);
	
	$name = 'theme_bootstrapcustom/logo_url';
    $title = get_string('logo_url','theme_bootstrapcustom');
    $description = get_string('logo_urldesc', 'theme_bootstrapcustom');
    $setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
    $settings->add($setting);
    
    $name = 'theme_bootstrapcustom/navlogo_url';
    $title = get_string('navlogo_url','theme_bootstrapcustom');
    $description = get_string('navlogo_urldesc', 'theme_bootstrapcustom');
    $setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
    $settings->add($setting);
    
    $name = 'theme_bootstrapcustom/navlogo_width';
    $title = get_string('navlogo_width','theme_bootstrapcustom');
    $description = get_string('navlogo_widthdesc', 'theme_bootstrapcustom');
    $default = 40;
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $settings->add($setting);
    
    $name = 'theme_bootstrapcustom/navlogo_height';
    $title = get_string('navlogo_height','theme_bootstrapcustom');
    $description = get_string('navlogo_heightdesc', 'theme_bootstrapcustom');
    $default = 40;
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $settings->add($setting);
    
    $name = 'theme_bootstrapcustom/customcss';
    $title = get_string('customcss','theme_bootstrapcustom');
    $description = get_string('customcssdesc', 'theme_bootstrapcustom');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $settings->add($setting);
    
    $name = 'theme_bootstrapcustom/gakey';
	$title = get_string('gakey','theme_bootstrapcustom');
	$description = get_string('gakeydesc', 'theme_bootstrapcustom');
	$setting = new admin_setting_configtext($name, $title, $description, '');
	$settings->add($setting);

}
