<?php

$hasheading = ($PAGE->heading);
$hasnavbar = (empty($PAGE->layout_options['nonavbar']) && $PAGE->has_navbar());
$hasfooter = (empty($PAGE->layout_options['nofooter']));
$hassidepre = $PAGE->blocks->region_has_content('side-pre', $OUTPUT);
$hassidepost = $PAGE->blocks->region_has_content('side-post', $OUTPUT);
$showsidepre = $hassidepre && !$PAGE->blocks->region_completely_docked('side-pre', $OUTPUT);
$showsidepost = $hassidepost && !$PAGE->blocks->region_completely_docked('side-post', $OUTPUT);
$custommenu = $OUTPUT->custom_menu();
$hascustommenu = (empty($PAGE->layout_options['nocustommenu']) && !empty($custommenu));

$bodyclasses = array();
if ($showsidepre && !$showsidepost) {
    $bodyclasses[] = 'side-pre-only';
} else if ($showsidepost && !$showsidepre) {
    $bodyclasses[] = 'side-post-only';
} else if (!$showsidepost && !$showsidepre) {
    $bodyclasses[] = 'content-only';
}
if ($hascustommenu) {
    $bodyclasses[] = 'has_custom_menu';
}
if ($hasnavbar) {
    $bodyclasses[] = 'hasnavbar';
}

echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes() ?>>
<head>
    <title><?php echo $PAGE->title ?></title>
    <?php echo $OUTPUT->standard_head_html() ?>
</head>
<body id="<?php p($PAGE->bodyid) ?>" class="<?php p($PAGE->bodyclasses.' '.join(' ', $bodyclasses)) ?>">
<?php echo $OUTPUT->standard_top_of_body_html() ?>


<?php if ($hasheading || $hasnavbar) { ?>
    <div id="header">
       <div id="header-topo"> 
	   <a href="<?php echo $CFG->wwwroot; ?>/course/view.php?id=<?php echo $COURSE->id?>" title="<?php echo $PAGE->heading ?>">     
		  <img src="<?php echo $OUTPUT->pix_url('header', 'theme')?>">
	   </a>
      </div>
    </div>
<?php } ?>
<!-- END OF HEADER -->

<div id="page">
	<?php if ($hasnavbar) { ?>
	<div class="navbar clearfix">
		<div class="breadcrumb"><?php echo $OUTPUT->navbar(); ?></div>
		<div class="navbutton"><?php echo $PAGE->button; ?></div>
	</div>
	<?php } ?>
    <div id="page-content">

        <div id="region-main-box">
            <div id="region-post-box">

                <div id="region-main-wrap">
                    <div id="region-main">
                        <div class="region-content">
 		      	   <div id="conteudo-centro">
                            <?php echo $OUTPUT->main_content() ?>
                           </div>
                        </div>
                    </div>
                </div>

                <?php if ($hassidepre) { ?>
                <div id="region-pre" class="block-region">
                    <div class="region-content">
                        <?php echo $OUTPUT->blocks_for_region('side-pre') ?>
                    </div>
                </div>
                <?php } ?>

                <?php if ($hassidepost) { ?>
                <div id="region-post" class="block-region">
                    <div class="region-content">
                        <?php echo $OUTPUT->blocks_for_region('side-post') ?>
                    </div>
                </div>
                <?php } ?>

            </div>
        </div>
    </div>
  <div class="clearfix"></div>
</div>

<!-- START OF FOOTER -->
  <?php if ($hasfooter) { ?>
    <div id="footer" class="clearfix">
       <div id="footer-rodape">
	<!-- FOOTER IMAGE -->
			<?php echo $OUTPUT->login_info(); ?>
     </div>
    </div>
  <?php } ?>

<?php echo $OUTPUT->standard_end_of_body_html() ?>
</body>
</html>
