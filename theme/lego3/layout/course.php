<?php

$hasheading = ($PAGE->heading);
$hasnavbar = (empty($PAGE->layout_options['nonavbar']) && $PAGE->has_navbar());
$hasfooter = (empty($PAGE->layout_options['nofooter']));
$hassidepre = $PAGE->blocks->region_has_content('side-pre', $OUTPUT);
$showsidepre = $hassidepre && !$PAGE->blocks->region_completely_docked('side-pre', $OUTPUT);
$custommenu = $OUTPUT->custom_menu();
$hascustommenu = (empty($PAGE->layout_options['nocustommenu']) && !empty($custommenu));

$bodyclasses = array();
if (!$showsidepre) {
    $bodyclasses[] = 'content-only';
}
if ($hascustommenu) {
    $bodyclasses[] = 'has_custom_menu';
}
if ($hasnavbar) {
    $bodyclasses[] = 'hasnavbar';
}

echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes() ?>>
<head>
    <title><?php echo $PAGE->title ?></title>
    <link rel="shortcut icon" href="<?php echo $OUTPUT->pix_url('favicon', 'theme')?>" />
	<link href='http://fonts.googleapis.com/css?family=Electrolize' rel='stylesheet' type='text/css'>
    <?php echo $OUTPUT->standard_head_html() ?>
</head>
<body id="<?php p($PAGE->bodyid) ?>" class="<?php p($PAGE->bodyclasses.' '.join(' ', $bodyclasses)) ?>">
<?php echo $OUTPUT->standard_top_of_body_html() ?>

<?php if ($hasheading || $hasnavbar) { ?>
    <div id="header">
       <div id="header-topo">
			<div id="logo-fafe">   
			   <a href="<?php echo $CFG->wwwroot; ?>/course/view.php?id=<?php echo $COURSE->id?>" title="<?php echo $PAGE->heading ?>">     
				  <img src="<?php echo $OUTPUT->pix_url('logo_fafe', 'theme')?>">
			   </a>
			</div>
			<div id="nome-moodle">
			   <?php if ($hasheading) { ?>
					<h1 class="headermain"><?php echo $PAGE->heading ?></h1>
			   <?php } ?>
			</div>
			<div id="profile-topo">
			   <?php include('profileblock.php')?>
			</div>	
			<?php if ($hasnavbar) { ?>
				<div class="navbar clearfix">
					<div class="breadcrumb"><?php echo $OUTPUT->navbar(); ?></div>
					<div class="navbutton"><?php echo $PAGE->button; ?></div>
				</div>
			<?php } ?>
      </div>
    </div>
<?php } ?>
<!-- END OF HEADER -->

<div id="page-topo"></div>
<div id="page">
    <div id="page-content">
        <div id="region-main-box">
            <div id="region-post-box">

                <div id="region-main-wrap">
                    <div id="region-main">
                        <div class="region-content">
 		      	   <div id="conteudo-centro">
                            <?php echo $OUTPUT->main_content() ?>
                           </div>
                        </div>
                    </div>
                </div>

                <?php if ($hassidepre) { ?>
                <div id="region-pre" class="block-region">
                    <div class="region-content">
                        <?php echo $OUTPUT->blocks_for_region('side-pre') ?>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
  <div class="clearfix"></div>
</div>
<div id="page-rodape"></div>

<!-- START OF FOOTER -->
  <?php if ($hasfooter) { ?>
    <div id="footer" class="clearfix">
       <div id="footer-rodape">
	  <div id="info">
		<?php
		echo $OUTPUT->login_info();
		echo $OUTPUT->standard_footer_html();
		?>
	 </div>
	<div id="logos">
	   <a href="<?php echo $CFG->wwwroot; ?>" title="Projeto Lego3Zoom">     
	      <img src="<?php echo $OUTPUT->pix_url('logo_zoom_footer', 'theme')?>" id="logo-lego3zoom-fafe">
	   </a>
	</div>
     </div>
    </div>
  <?php } ?>

<?php echo $OUTPUT->standard_end_of_body_html() ?>
</body>
</html>
