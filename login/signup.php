<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * user signup page.
 *
 * @package    core
 * @subpackage auth
 * @copyright  1999 onwards Martin Dougiamas  http://dougiamas.com
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../config.php');
global $DB;

$ac = optional_param('accesscode','',PARAM_ALPHANUM);
$sql = 'SELECT * FROM mdl_block_accesscode_codes WHERE accesscode = ? AND  userid = 0';
$acvalid = $DB->record_exists_sql($sql, array($ac));


if (empty($CFG->registerauth)) {
    print_error('notlocalisederrormessage', 'error', '', 'Sorry, you may not use this page.');
}
$authplugin = get_auth_plugin($CFG->registerauth);

if (!$authplugin->can_signup()) {
    print_error('notlocalisederrormessage', 'error', '', 'Sorry, you may not use this page.');
}

//HTTPS is required in this page when $CFG->loginhttps enabled
$PAGE->https_required();

$PAGE->set_url('/login/signup.php');
$PAGE->set_context(context_system::instance());

$mform_signup = $authplugin->signup_form();

if (!$ac && !$mform_signup->get_data()) {
    redirect(get_login_url());
}

$toform['accesscode'] = $ac;
$mform_signup->set_data($toform);

if ($mform_signup->is_cancelled()) {
    redirect(get_login_url());

} else if ($data = $mform_signup->get_data()) {


    $user = new stdClass;
    $user->auth        = 'manual';
    $user->confirmed   = 1;
    $user->mnethostid  = $CFG->mnet_localhost_id;
    $user->username    = $data->email;
    $user->password    = $data->password;
    $user->idnumber    = $data->cpf;
    $user->firstname   = $data->firstname;
    $user->lastname    = $data->lastname;
    $user->email       = $data->email;
    $user->icq         = $data->rg;
    $user->address     = $data->logradouro . ', ' . $data->numero . ' ' . $data->complemento . ' - ' . $data->bairro . ' CEP' . $data->cep;
    $user->city        = $data->city . ' - ' . $data->estado;
    $user->country     = $data->country;
    $user->lang        = current_language();
    $user->firstaccess = time();
    $user->timecreated = time();
    $user->accesscode  = $data->accesscode;


    $authplugin->user_signup($user, true); // prints notice and link to login/index.php
    exit; //never reached
}

// make sure we really are on the https page when https login required
$PAGE->verify_https_required();

if(!$acvalid){
    redirect(get_login_url());
}

$newaccount = get_string('newaccount');
$login      = get_string('login');

$PAGE->navbar->add($login);
$PAGE->navbar->add($newaccount);

$PAGE->set_title($newaccount);
$PAGE->set_heading($SITE->fullname);

echo $OUTPUT->header();
$mform_signup->display();
echo $OUTPUT->footer();
