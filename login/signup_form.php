<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * User sign-up form.
 *
 * @package    core
 * @subpackage auth
 * @copyright  1999 onwards Martin Dougiamas  http://dougiamas.com
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/formslib.php');
require_once($CFG->dirroot.'/user/profile/lib.php');

class login_signup_form extends moodleform {
    function definition() {
        global $USER, $CFG;

        $mform = $this->_form;

        $mform->addElement('header', '', 'Informações de acesso', '');

        $mform->addElement('static', 'passwordpolicyinfo', '', 'O endereço de email e senha inseridos abaixo serão utilizados para acessar a plataforma');

        //Email
        $mform->addElement('text', 'email', get_string('email'), 'maxlength="100" size="25"');
        $mform->setType('email', PARAM_NOTAGS);
        $mform->addRule('email', get_string('missingemail'), 'required', null, 'server');

        //Confirmação de Email
        $mform->addElement('text', 'email2', get_string('emailagain'), 'maxlength="100" size="25"');
        $mform->setType('email2', PARAM_NOTAGS);
        $mform->addRule('email2', get_string('missingemail'), 'required', null, 'server');

        //Senha
        if (!empty($CFG->passwordpolicy)){
            $mform->addElement('static', 'passwordpolicyinfo', '', print_password_policy());
        }
        $mform->addElement('passwordunmask', 'password', get_string('password'), 'maxlength="32" size="12"');
        $mform->setType('password', PARAM_RAW);
        $mform->addRule('password', get_string('missingpassword'), 'required', null, 'server');

        $mform->addElement('header', '', 'Informações pessoais', '');

        //Nome
        $mform->addElement('static', 'passwordpolicyinfo', '', 'Preencha com cuidado os campos abaixo pois as informações serão utilizadas na confecção dos certificados');
        $mform->addElement('text', 'firstname', get_string('firstname'), 'maxlength="100" size="30"');
        $mform->setType('firstname', PARAM_TEXT);
        $mform->addRule('firstname', get_string('missingfirstname'), 'required', null, 'server');

        //Sobrenome
        $mform->addElement('text', 'lastname',  get_string('lastname'),  'maxlength="100" size="30"');
        $mform->setType('lastname', PARAM_TEXT);
        $mform->addRule('lastname', get_string('missinglastname'), 'required', null, 'server');

        //CPF
        $mform->addElement('text', 'cpf',  'CPF (somente números)',  'maxlength="11" size="11"');
        $mform->setType('cpf', PARAM_TEXT);
        $mform->addRule('cpf', get_string('missinglastname'), 'required', null, 'server');

        //RG
        $mform->addElement('text', 'rg',  'RG (somente números)',  'maxlength="9" size="9"');
        $mform->setType('rg', PARAM_TEXT);
        $mform->addRule('rg', get_string('missinglastname'), 'required', null, 'server');

        $mform->addElement('header', '', 'Endereço', '');

        //Endereco
        $mform->addElement('text', 'logradouro',  'Logradouro',  'maxlength="200" size="60"');
        $mform->setType('logradouro', PARAM_TEXT);
        $mform->addRule('logradouro', get_string('missinglastname'), 'required', null, 'server');

        //Número
        $mform->addElement('text', 'numero',  'Número',  'maxlength="6" size="6"');
        $mform->setType('numero', PARAM_TEXT);
        $mform->addRule('numero', get_string('missinglastname'), 'required', null, 'server');

        //Complemento
        $mform->addElement('text', 'complemento',  'Complemento',  'maxlength="20" size="10"');
        $mform->setType('complemento', PARAM_TEXT);


        //Cidade
        $mform->addElement('text', 'city', get_string('city'), 'maxlength="120" size="40"');
        $mform->setType('city', PARAM_TEXT);
        $mform->addRule('city', get_string('missingcity'), 'required', null, 'server');
        if (!empty($CFG->defaultcity)) {
            $mform->setDefault('city', $CFG->defaultcity);
        }

        //Bairro
        $mform->addElement('text', 'bairro',  'Bairro',  'maxlength="100" size="40"');
        $mform->setType('bairro', PARAM_TEXT);
        $mform->addRule('bairro', get_string('missinglastname'), 'required', null, 'server');

        //CEP
        $mform->addElement('text', 'cep',  'CEP',  'maxlength="9" size="9"');
        $mform->setType('cep', PARAM_TEXT);
        $mform->addRule('cep', get_string('missinglastname'), 'required', null, 'server');

        //Estado
        $mform->addElement('text', 'estado',  'Estado',  'maxlength="2" size="2"');
        $mform->setType('estado', PARAM_TEXT);
        $mform->addRule('estado', get_string('missinglastname'), 'required', null, 'server');

        $country = get_string_manager()->get_list_of_countries();
        $default_country[''] = get_string('selectacountry');
        $country = array_merge($default_country, $country);
        $mform->addElement('select', 'country', get_string('country'), $country);
        $mform->addRule('country', get_string('missingcountry'), 'required', null, 'server');

        if( !empty($CFG->country) ){
            $mform->setDefault('country', $CFG->country);
        }else{
            $mform->setDefault('country', '');
        }

        if ($this->signup_captcha_enabled()) {
            $mform->addElement('recaptcha', 'recaptcha_element', get_string('recaptcha', 'auth'), array('https' => $CFG->loginhttps));
            $mform->addHelpButton('recaptcha_element', 'recaptcha', 'auth');
        }

        profile_signup_fields($mform);

        if (!empty($CFG->sitepolicy)) {
            $mform->addElement('header', '', get_string('policyagreement'), '');
            $mform->addElement('static', 'policylink', '', '<a href="'.$CFG->sitepolicy.'" onclick="this.target=\'_blank\'">'.get_String('policyagreementclick').'</a>');
            $mform->addElement('checkbox', 'policyagreed', get_string('policyaccept'));
            $mform->addRule('policyagreed', get_string('policyagree'), 'required', null, 'server');
        }

        $mform->addElement('hidden', 'accesscode');

        // buttons
        $this->add_action_buttons(true, 'Criar usuário');

    }

    function definition_after_data(){
        $mform = $this->_form;
        $mform->applyFilter('username', 'trim');
    }

    function validation($data, $files) {
        global $CFG, $DB;
        $errors = parent::validation($data, $files);

        $authplugin = get_auth_plugin($CFG->registerauth);

        if ($DB->record_exists('user', array('username'=>$data['email'], 'mnethostid'=>$CFG->mnet_localhost_id))) {
            $errors['username'] = get_string('usernameexists');
        }

        if (! validate_email($data['email'])) {
            $errors['email'] = get_string('invalidemail');

        } else if ($DB->record_exists('user', array('email'=>$data['email']))) {
            $errors['email'] = get_string('emailexists').' <a href="forgot_password.php">'.get_string('newpassword').'?</a>';
        }
        if (empty($data['email2'])) {
            $errors['email2'] = get_string('missingemail');

        } else if ($data['email2'] != $data['email']) {
            $errors['email2'] = get_string('invalidemail');
        }
        if (!isset($errors['email'])) {
            if ($err = email_is_not_allowed($data['email'])) {
                $errors['email'] = $err;
            }

        }

        $errmsg = '';
        if (!check_password_policy($data['password'], $errmsg)) {
            $errors['password'] = $errmsg;
        }

        if ($this->signup_captcha_enabled()) {
            $recaptcha_element = $this->_form->getElement('recaptcha_element');
            if (!empty($this->_form->_submitValues['recaptcha_challenge_field'])) {
                $challenge_field = $this->_form->_submitValues['recaptcha_challenge_field'];
                $response_field = $this->_form->_submitValues['recaptcha_response_field'];
                if (true !== ($result = $recaptcha_element->verify($challenge_field, $response_field))) {
                    $errors['recaptcha'] = $result;
                }
            } else {
                $errors['recaptcha'] = get_string('missingrecaptchachallengefield');
            }
        }
        // Validate customisable profile fields. (profile_validation expects an object as the parameter with userid set)
        $dataobject = (object)$data;
        $dataobject->id = 0;
        $errors += profile_validation($dataobject, $files);

        return $errors;

    }

    /**
     * Returns whether or not the captcha element is enabled, and the admin settings fulfil its requirements.
     * @return bool
     */
    function signup_captcha_enabled() {
        global $CFG;
        return !empty($CFG->recaptchapublickey) && !empty($CFG->recaptchaprivatekey) && get_config('auth/email', 'recaptcha');
    }

}
